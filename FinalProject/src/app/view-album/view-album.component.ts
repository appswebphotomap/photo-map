import {Component, OnInit} from '@angular/core';
import {AlbumDataService} from '../album-data.service';
import {ActivatedRoute} from '@angular/router';
import * as $ from 'jquery';
import {HttpClient} from '@angular/common/http';

interface userData{
    id: string;
    email: string;
    password:string;
    albums: Album[];

}

interface Album{
    id: string;
    title: string;
    description: string;
    date: string;
    authors: string[];
    photos:Photo[];

}
interface Photo{
    id: string;
    url:string;
    title: string;
    comment:string;
    latitude:string;
    longitude:string;
    metadata:Object;
}


@Component({
    selector: 'app-view-album',
    templateUrl: './view-album.component.html',
    styleUrls: ['./view-album.component.scss']
})
export class ViewAlbumComponent implements OnInit {

    //http aux variables
    userAlbums =  [] as Array<Album>;

    albumData;
    albumName;
    photos = [];
    myConfig: any = {
        masonry: true,
        masonryMaxHeight: 195,
        masonryGutter: 6,
        loop: false,
        backgroundOpacity: 0.85,
        animationDuration: 100,
        counter: true,
        lightboxMaxHeight: '100vh - 86px',
        lightboxMaxWidth: '100%'
    };

    selectedTabIsMap = true;

    constructor(private service: AlbumDataService, private route: ActivatedRoute, private http: HttpClient) {
        this.albumData = this.service.getAlbums();
        for (let i = 15; i <= 130; i++) {
            const img = new Image();
            const ctx = this;
            img.addEventListener('load', function () {
                const imageObj = {
                    preview: 'http://www.brunoschalch.com/timeuntil/appresources/thumbnail/' + i + '.jpg',
                    full: 'http://www.brunoschalch.com/timeuntil/appresources/large/' + i + '.jpg',
                    width: this.naturalWidth, // used for masonry
                    height: this.naturalHeight, // used for masonry
                 //   description: 'description of the image' // optional property
                };
                ctx.photos.push(imageObj);
            });
            img.src = 'http://www.brunoschalch.com/timeuntil/appresources/thumbnail/' + i + '.jpg';
        }

        this.route.queryParams.subscribe(params => {
            console.log(params);
            this.albumName = params['albumName'];
        });

        //console.log(this.userAlbums);


    }
    getAlbumData(){
        // Service request using loobback, user id needed.
        this.userAlbums = this.service.getUserAlbums();
        console.log(this.userAlbums);
    }

    clickedTab(tab) {
        if (tab === 'map') {
            this.selectedTabIsMap = true;
        } else {
            this.selectedTabIsMap = false;
            this.getAlbumData();

        }
    }

    ngOnInit() {
    }

    shareClicked() {
      // get current album
      const shareAlbum: Album = this.service.userData.albums[12];
      // get list of allowed users, if any
      const allowedUsers: string[] = [];
      this.service.shareAlbum(shareAlbum, allowedUsers).then((result) => {
        console.log('Album shared');
        console.log(result);
      }).catch((reason) => {
        console.log('Album share failed');
        console.log(reason);
      });
      this.showToast();
    }

    showToast() {
        // Get the snackbar DIV
        const x = document.getElementById('snackbar');

        // Add the "show" class to DIV
        x.className = 'show';

        // After 3 seconds, remove the show class from DIV
        setTimeout(function () {
            x.className = x.className.replace('show', '');
        }, 3000);
    }
}
