import { Component, OnInit } from '@angular/core';
import { AlbumDataService } from '../album-data.service';
import { FormControl } from '@angular/forms';
import {Router, NavigationExtras} from '@angular/router';
import {FileSystemDirectoryEntry, FileSystemFileEntry, UploadEvent, UploadFile} from 'ngx-file-drop';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { NgxSpinnerService } from 'ngx-spinner';
import ImageCompressor from 'image-compressor.js';
import * as EXIF from 'exif-js';



interface Photo{
    id: string;
    url:string;
    title: string;
    comment:string;
    latitude:string;
    longitude:string;
    metadata:Object;
}


@Component({
    selector: 'app-create-album',
    templateUrl: './create-album.component.html',
    styleUrls: ['./create-album.component.scss']
})
export class CreateAlbumComponent implements OnInit {
    constructor(public router: Router, private http: HttpClient, private service: AlbumDataService,  private spinner: NgxSpinnerService) { }

    albumName = new FormControl('');
    albumDescription = new FormControl('');
    //photo array to be send to service method
    albumPhotos = [] as Array<Photo>;




    files: File[] = [];
    compressedImages: any[] = [];
    exifs: any[] = [];




    // Sends photos to service to generate a new album
    navigateToViewAlbum() {

        let navigationExtras: NavigationExtras = {
            queryParams: {
                "albumName": this.albumName.value
            }
        };

        this.router.navigate( ["view"] , navigationExtras);
    }


    ngOnInit() {
    }

    openNav() {
        document.getElementById("mySidenav").style.width = "60%";
    }
    closeNav() {
        document.getElementById("mySidenav").style.width = "0";
    }

    public dropped(event: UploadEvent) {
        //nope  this.files = event.files;
      this.compressedImages = [];
      this.files = [];

        for (const droppedFile of event.files) {

            // Is it a file?
            if (droppedFile.fileEntry.isFile) {
                const fileEntry = droppedFile.fileEntry as FileSystemFileEntry;
                fileEntry.file((file: File) => {

                    // It must be a jpeg
                    if (file.name.toLowerCase().substring(file.name.length - 3, file.name.length) === 'jpg'
                        || file.name.toLowerCase().substring(file.name.length - 4, file.name.length) === 'jpeg') {
                      this.getExifAndCompress(file, this);
                      this.files.push(file);
                    }
                    // Here you can access the real file
                    console.log(droppedFile.relativePath, file);

                    /**
           // You could upload it like this:
           const formData = new FormData()
           formData.append('logo', file, relativePath)

           // Headers
           const headers = new HttpHeaders({
            'security-token': 'mytoken'
          })

           this.http.post('https://mybackend.com/api/upload/sanitize-and-save-logo', formData, { headers: headers, responseType: 'blob' })
           .subscribe(data => {
            // Sanitized logo returned from backend
          })
           **/

                });
            } else {
                // It was a directory (empty directories are added, otherwise only files)
                const fileEntry = droppedFile.fileEntry as FileSystemDirectoryEntry;
                console.log(droppedFile.relativePath, fileEntry);
            }
        }
    }

    public fileOver(event){
        console.log(event);
    }

    public fileLeave(event){
        console.log(event);
    }

    /*
    Receives an array of files to be uploaded, all jpg, and returns an array of strings to the link of uploaded files.
     */
    public uploadFiles(inputs, callback) {
        let counter = 0;
        const photoURLs = [];
        const options = {
            headers: new HttpHeaders().set('Content-Type', 'image/jpeg'),
        };

        for (const currentFile of inputs) {
            // Final link always looks like https://storage.googleapis.com/photomapbucket/[NAME].jpg
            const rndName =  Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
            const name = rndName + '.jpg';
            this.http.post('https://www.googleapis.com/upload/storage/v1/b/photomapbucket/o?uploadType=media&name=' + name, currentFile.file, options).toPromise().then((value: any) => {
                console.log(value);
                photoURLs.push({
                  'url': 'https://storage.googleapis.com/photomapbucket/' + name,
                  'metadata': currentFile.exif,
                  }
                  );
                counter++;
                // Check if this is the final file, if it is, do next thing.
                // Angel: photoURLs
                if (counter === inputs.length) {
                    callback(photoURLs);
                }
            }).catch(reason => {
                counter++;
                console.log(reason);
            } );

        }

    }

  static compressImage(file: File, callback) {
      const quality = (<HTMLInputElement>document.getElementsByName('qualityslider')[0]).value;
      let qltfloat = parseFloat(quality);
      if (qltfloat > 0.7 || qltfloat < 0.0) {
        qltfloat = 0.7;
      }
    const imgCmp = new ImageCompressor(file, {
      quality: qltfloat,
      success(result) {
        console.log('does this have metadata?');
        console.log(result);
        callback(result);
        //     context.pushImage(result);
        //  this.images.push(result);
      },
      error(e) {
        console.log(e);
      },
    });
  }

  getExifAndCompress(image, context) {
    EXIF.getData(image, function() {
      console.log('exif data extracted');
      const longArray = EXIF.getTag(image, 'GPSLongitude');
      const latArray = EXIF.getTag(image, 'GPSLatitude');
      const longitude = -CreateAlbumComponent.GPStoDecimal(longArray);
      const latitude = CreateAlbumComponent.GPStoDecimal(latArray);

      const exifData = image.exifdata;
      console.log(exifData);
      // Now that metadata has been extracted and isolated, compress. Push metadata and image simultaneously.
      CreateAlbumComponent.compressImage(image, (compressedImg) => {
        context.compressedImages.push(
          {
            'file': compressedImg,
            'exif': exifData,
            'longitude': longitude,
            'latitude': latitude,
          }
        );
      });
    });
  }

  static GPStoDecimal(number): number {
    return number[0].numerator + number[1].numerator /
      (60 * number[1].denominator) + number[2].numerator / (3600 * number[2].denominator);
  }



    submitClicked() {

        if(this.compressedImages.length === this.files.length){
          this.spinner.show();
            console.log('Trying to upload the following files: ');
            console.log(this.compressedImages);
            // This will take care of upl
            this.uploadFiles(this.compressedImages, (images) => {
                // Angel: right here you get all the photoURLs in an array.
                for (const image of images){
                    let tempPhoto =  {} as Photo;
                    const rndId =  Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
                    tempPhoto.id = rndId;
                    tempPhoto.url = image.url;
                    const rndName =  Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
                    tempPhoto.title = rndName;
                    tempPhoto.comment = '';
                    tempPhoto.latitude = image.latitude;
                    tempPhoto.longitude = image.longitude;
                    tempPhoto.metadata = image.metadata;

                    this.albumPhotos.push(tempPhoto);
                }
                let description = (<HTMLInputElement>document.getElementById("description")).value;
                console.log(images);
                this.service.addUserAlbumPhotos(this.albumName.value, description ,this.albumPhotos ) ;
              this.spinner.hide();

               this.navigateToViewAlbum();

            });
        }
    }


}
