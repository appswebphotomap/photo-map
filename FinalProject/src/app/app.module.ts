import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { MenuBarComponent } from './menu-bar/menu-bar.component';
import { FooterComponent } from './footer/footer.component';
import { ModalSigninComponent } from './modal-signin/modal-signin.component';
import { MainComponent } from './main/main.component';
import { ViewAlbumComponent } from './view-album/view-album.component';
import { CreateAlbumComponent } from './create-album/create-album.component';
import { LoginComponent } from './login/login.component';
import { FormsModule } from '@angular/forms';
import { CrystalGalleryModule } from 'ngx-crystal-gallery';
import { FileDropModule } from 'ngx-file-drop';
import { HttpClientModule } from '@angular/common/http';
import { ChartService } from './chart.service';

const appRoutes = [
  { path: '', component: MainComponent },
  { path: 'login', component: LoginComponent },
  { path: 'create', component: CreateAlbumComponent },
  { path: 'view', component: ViewAlbumComponent },
];

@NgModule({
  declarations: [
    AppComponent,
    MenuBarComponent,
    FooterComponent,
    ModalSigninComponent,
    MainComponent,
    ViewAlbumComponent,
    CreateAlbumComponent,
    LoginComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(
      appRoutes
    ),
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    FileDropModule,
    NgxSpinnerModule,
    CrystalGalleryModule,
  ],
  providers: [ChartService],
  bootstrap: [AppComponent]
})
export class AppModule { }
