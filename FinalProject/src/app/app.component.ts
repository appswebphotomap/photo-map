import { Component } from '@angular/core';
import { ChartService } from './chart.service';
import { Chart } from 'chart.js';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'PhotoMap';
  chart = [];

  constructor(private _data: ChartService){}

  /*ngOnInit(){
    this._data.getChartData()
      .subscribe(res => {
        console.log(res);
        //get actual data
      })
  }*/
}
