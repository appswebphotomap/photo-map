import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
//import {Observable} from 'rxjs/observable';

interface UserData{
    id: string;
    email: string;
    password:string;
    albums: Album[];
}
interface Album{
    id: string;
    title: string;
    description: string;
    date: string;
    authors: string[];
    photos:Photo[];

}
interface PublicAlbum {
    id: string;
    title: string;
    date: string;
    authors: string[];
    allowedusers: string[];
    photos: Photo[];

}
interface Photo{
    id: string;
    url:string;
    title: string;
    comment:string;
    latitude:string;
    longitude:string;
    metadata:Object;
}

@Injectable({
    providedIn: 'root'
})

export class AlbumDataService {
  album = {
        'name': 'Vacation',
        'description': 'Everyone is happy during holidays! Yay!',
        'date': '2018-04-30T17:37:22.735Z',
        'authors': [
            {
                '_id': '5ae75452d67d76001070a030',
                'name': 'John Moran',
            },
            {
                '_id': '5ae75452d67d76001070a030',
                'name': 'John Moran',
            },
            {
                '_id': '5ae75452d67d76001070a030',
                'name': 'John Moran',
            },
            {
                '_id': '5ae75452d67d76001070a030',
                'name': 'John Moran',
            },
        ],
        'photos': [
            {
                'name': 'photo1',
                'description': 'this is a test photo',
                'url': '../assets/img/stock1.jpg',
                'metaData': {
                    'SubSecTime': '266832',
                    'GPSLatitudeRef': null,
                    'WhiteBalance': '0',
                    'GPSDateStamp': null,
                    'GPSLatitude': '19/1,29/1,3289947/1000000',
                    'GPSAltitude': '221134/100',
                    'GPSAltitudeRef': null,
                    'GPSLongitude': null,
                    'SubSecTimeDigitized': '266832',
                    'DateTime': '2018:05:18 14:35:09',
                    'GPSLongitudeRef': null,
                    'FocalLength': '3830/1000',
                    'Flash': '0',
                    'ImageLength': '2240',
                    'ExposureTime': '9.44E-4',
                    'Make': 'HUAWEI',
                    'FNumber': '2.2',
                    'GPSProcessingMethod': null,
                    'DateTimeDigitized': '2018:05:18 14:35:09',
                    'GPSTimeStamp': null,
                    'ImageWidth': '3968',
                    'SubSecTimeOriginal': '266832',
                    'ISOSpeedRatings': '50',
                    'Model': 'BLL-L23',
                    'Orientation': '1'
                },
            },
            {
                'name': 'photo2',
                'description': 'this is a test photo',
                'url': '../assets/img/stock2.jpg',
                'metaData': {
                    'SubSecTime': '266832',
                    'GPSLatitudeRef': null,
                    'WhiteBalance': '0',
                    'GPSDateStamp': null,
                    'GPSLatitude': '19/1,29/1,3289947/1000000',
                    'GPSAltitude': '221134/100',
                    'GPSAltitudeRef': null,
                    'GPSLongitude': null,
                    'SubSecTimeDigitized': '266832',
                    'DateTime': '2018:05:18 14:35:09',
                    'GPSLongitudeRef': null,
                    'FocalLength': '3830/1000',
                    'Flash': '0',
                    'ImageLength': '2240',
                    'ExposureTime': '9.44E-4',
                    'Make': 'HUAWEI',
                    'FNumber': '2.2',
                    'GPSProcessingMethod': null,
                    'DateTimeDigitized': '2018:05:18 14:35:09',
                    'GPSTimeStamp': null,
                    'ImageWidth': '3968',
                    'SubSecTimeOriginal': '266832',
                    'ISOSpeedRatings': '50',
                    'Model': 'BLL-L23',
                    'Orientation': '1'
                },
            },
            {
                'name': 'photo2',
                'description': 'this is a test photo',
                'url': '../assets/img/stock3.jpg',
                'metaData': {
                    'SubSecTime': '266832',
                    'GPSLatitudeRef': null,
                    'WhiteBalance': '0',
                    'GPSDateStamp': null,
                    'GPSLatitude': '19/1,29/1,3289947/1000000',
                    'GPSAltitude': '221134/100',
                    'GPSAltitudeRef': null,
                    'GPSLongitude': null,
                    'SubSecTimeDigitized': '266832',
                    'DateTime': '2018:05:18 14:35:09',
                    'GPSLongitudeRef': null,
                    'FocalLength': '3830/1000',
                    'Flash': '0',
                    'ImageLength': '2240',
                    'ExposureTime': '9.44E-4',
                    'Make': 'HUAWEI',
                    'FNumber': '2.2',
                    'GPSProcessingMethod': null,
                    'DateTimeDigitized': '2018:05:18 14:35:09',
                    'GPSTimeStamp': null,
                    'ImageWidth': '3968',
                    'SubSecTimeOriginal': '266832',
                    'ISOSpeedRatings': '50',
                    'Model': 'BLL-L23',
                    'Orientation': '1'
                },
            },
            {
                'name': 'photo2',
                'description': 'this is a test photo',
                'url': '../assets/img/stock4.png',
                'metaData': {
                    'SubSecTime': '266832',
                    'GPSLatitudeRef': null,
                    'WhiteBalance': '0',
                    'GPSDateStamp': null,
                    'GPSLatitude': '19/1,29/1,3289947/1000000',
                    'GPSAltitude': '221134/100',
                    'GPSAltitudeRef': null,
                    'GPSLongitude': null,
                    'SubSecTimeDigitized': '266832',
                    'DateTime': '2018:05:18 14:35:09',
                    'GPSLongitudeRef': null,
                    'FocalLength': '3830/1000',
                    'Flash': '0',
                    'ImageLength': '2240',
                    'ExposureTime': '9.44E-4',
                    'Make': 'HUAWEI',
                    'FNumber': '2.2',
                    'GPSProcessingMethod': null,
                    'DateTimeDigitized': '2018:05:18 14:35:09',
                    'GPSTimeStamp': null,
                    'ImageWidth': '3968',
                    'SubSecTimeOriginal': '266832',
                    'ISOSpeedRatings': '50',
                    'Model': 'BLL-L23',
                    'Orientation': '1'
                },
            },
            {
                'name': 'photo2',
                'description': 'this is a test photo',
                'url': '../assets/img/stock5.jpg',
                'metaData': {
                    'SubSecTime': '266832',
                    'GPSLatitudeRef': null,
                    'WhiteBalance': '0',
                    'GPSDateStamp': null,
                    'GPSLatitude': '19/1,29/1,3289947/1000000',
                    'GPSAltitude': '221134/100',
                    'GPSAltitudeRef': null,
                    'GPSLongitude': null,
                    'SubSecTimeDigitized': '266832',
                    'DateTime': '2018:05:18 14:35:09',
                    'GPSLongitudeRef': null,
                    'FocalLength': '3830/1000',
                    'Flash': '0',
                    'ImageLength': '2240',
                    'ExposureTime': '9.44E-4',
                    'Make': 'HUAWEI',
                    'FNumber': '2.2',
                    'GPSProcessingMethod': null,
                    'DateTimeDigitized': '2018:05:18 14:35:09',
                    'GPSTimeStamp': null,
                    'ImageWidth': '3968',
                    'SubSecTimeOriginal': '266832',
                    'ISOSpeedRatings': '50',
                    'Model': 'BLL-L23',
                    'Orientation': '1'
                },
            }
        ]
    };
  albums = [
        {
            'name': 'Vacation',
            'description': 'Everyone is happy during holidays! Yay!',
            'date': '2018-04-30T17:37:22.735Z',
            'authors': [
                {
                    '_id': '5ae75452d67d76001070a030',
                    'name': 'John Moran',
                },
                {
                    '_id': '5ae75452d67d76001070a030',
                    'name': 'John Moran',
                },
                {
                    '_id': '5ae75452d67d76001070a030',
                    'name': 'John Moran',
                },
                {
                    '_id': '5ae75452d67d76001070a030',
                    'name': 'John Moran',
                },
            ]
        },
        {
            'name': 'Work',
            'description': 'It takes some skill to enjoy the day to day hassle! Life is easy.',
            'date': '2018-04-30T17:37:22.735Z',
            'authors': [
                {
                    '_id': '5ae75452d67d76001070a030',
                    'name': 'John Moran',
                },
                {
                    '_id': '5ae75452d67d76001070a030',
                    'name': 'John Moran',
                },
                {
                    '_id': '5ae75452d67d76001070a030',
                    'name': 'John Moran',
                },
                {
                    '_id': '5ae75452d67d76001070a030',
                    'name': 'John Moran',
                },
            ]
        },
    ];
  userData = {} as UserData ;

  constructor(private http: HttpClient) {
    //User ID should send here, if no ID exists, abort and redirect to login page.
    this.saveUserData('bruno');
  }

  // methos used to retrive album from Lb API
  newUser(_id:string, _email:string, _password:string){
        let newUser = {} as UserData;
        newUser.id = _id;
        newUser.email = _email;
        newUser.password = _password;
        let newAlbum = [] as Array<Album>;
        newUser.albums = newAlbum;
        this.postNewUser(newUser).then((data) =>{
           console.log(data);
        });
  }

  postNewUser(newUser){
    return this.http.post<UserData>('http://127.0.0.1:3000/user-data/', newUser).toPromise();
  }

  getUserData(id){
    return this.http.get<UserData>('http://127.0.0.1:3000/user-data/' + id).toPromise();
  }

  saveUserData(id){
        this.getUserData(id).then((data )=>{
            // console.log(data);
            this.userData = data;
            // console.log(this.userData.albums);
        });
  }

  getUserAlbums() {
        console.log(this.userData);
        return this.userData.albums;
  }

  newAlbum : Album;
  addUserAlbumPhotos(name:string, description:string ,photos: Photo[]){
        // All album properties should be inserted
        let newAlbum = {} as Album;
        const rndId =  Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
        newAlbum.id = rndId;
        newAlbum.title = name;

        newAlbum.description = description;
        newAlbum.date = "2018-11-30T09:14:07.159Z"

        newAlbum.authors = [];
        newAlbum.photos = photos;
        this.userData.albums.push(newAlbum);
        this.uploadNewAlbum('bruno');
  }

  uploadNewAlbum(id){
        this.deleteUserData(id).then((data) =>{
            this.postUserData().then((data2) =>{
                console.log(this.userData);
                console.log(data2) ;
            });
        });
  }

  postUserData(){
    return this.http.post<UserData>('http://127.0.0.1:3000/user-data', this.userData).toPromise();
  }

  deleteUserData(id){
    return this.http.delete('http://127.0.0.1:3000/user-data/' + id).toPromise();
  }

  shareAlbum(album: Album, allowedUsers) {
    const allowedUsersObj = {'allowedusers': allowedUsers};
    // Spread magic (see https://visualstudiomagazine.com/articles/2018/02/28/copying-classes.aspx)
    //const myAlbumToShare: PublicAlbum = { ...album, ...allowedUsersObj };
    const myAlbumToShare = { ...album, ...allowedUsersObj } as PublicAlbum;
    return this.http.post<PublicAlbum>('http://127.0.0.1:3000/public-album', myAlbumToShare).toPromise();
  }
    // TODO(bruno): avoid using any as type in prod.
  getAlbums(): any {
    return this.albums;
  }

  getAlbum(id: string): any {
    return this.album;
  }
}
